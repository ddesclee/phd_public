import itertools

import numpy as np

from pandas import DataFrame

from chaco.api import (
    Plot, ArrayPlotData, ColorBar, HPlotContainer, LinearMapper, jet
)

from chaco.tools.api import PanTool, ZoomTool
from enable.api import Component, ComponentEditor
from traits.api import HasTraits, Str, Instance, Property, List, on_trait_change
from traitsui.api import View, Item, VGroup, HGroup, EnumEditor

class Viewer(HasTraits):

    dataframe = Instance(DataFrame)
    columns = Property(List, depends_on='dataframe')

    x_dim = Str
    y_dim = Str
    color_dim = Str

    _plot = Instance(Component)
    _data = Instance(ArrayPlotData)


    def __data_default(self):
        dataset = ArrayPlotData(
            x_dim=convert_column(self.dataframe[self.x_dim]),
            y_dim=convert_column(self.dataframe[self.y_dim]),
            color_dim=convert_column(self.dataframe[self.color_dim])
        )
        return dataset

    def __plot_default(self):
        plot = Plot(self._data)
        plot.plot(('x_dim', 'y_dim', 'color_dim'),
                  type='cmap_scatter', name='pandas_view',
                  color_mapper=jet,
                  marker='circle',
                  fill_alpha=0.5)
        plot.padding = 50
        plot.x_grid.visible = False
        plot.y_grid.visible = False
        plot.x_axis.font = "modern 16"
        plot.y_axis.font = "modern 16"
        plot.range2d.y_range.high = 'auto'
        plot.range2d.y_range.low = 'auto'
        plot.range2d.x_range.low = 'auto'
        plot.range2d.x_range.high = 'auto'

        cmap_renderer = plot.plots["pandas_view"][0]

        # Attach some tools to the plot
        plot.tools.append(PanTool(plot))
        zoom = ZoomTool(component=plot, tool_mode="box", always_on=False)
        plot.overlays.append(zoom)

        # Create the colorbar, handing in the appropriate range and colormap
        colorbar = create_colorbar(plot.color_mapper)
        colorbar.plot = cmap_renderer
        colorbar.padding_top = plot.padding_top
        colorbar.padding_bottom = plot.padding_bottom

        # Create a container to position the plot and the colorbar side-by-side
        container = HPlotContainer(use_backbuffer = True)
        container.add(plot)
        container.add(colorbar)
        container.bgcolor = "lightgray"

        return container


    def _get_columns(self):
        return self.dataframe.columns.values.tolist()

    @on_trait_change('x_dim,y_dim,color_dim', post_init=True)
    def _dimension_changed(self, obj, dimension, column_name):
        self._data.set_data(
            dimension, convert_column(self.dataframe[column_name])
        )


    traits_view = View(
        HGroup(
            VGroup(
                Item('x_dim', editor=EnumEditor(name='columns')),
                Item('y_dim', editor=EnumEditor(name='columns')),
                Item('color_dim', editor=EnumEditor(name='columns')),
            ),
            VGroup(
                Item('_plot', editor=ComponentEditor(), show_label=False)
            ),
        ),
        resizable=True,
        title='Interactive data viewer'
    )

def convert_column(column):
    if column.dtype is np.dtype('O'):
        # more than likely strings
        unique_values = column.unique()
        counter = itertools.count(0)
        mapping = { key:counter.next() for key in unique_values}
        values = [mapping[value] for value in column]
    else:
        values = column

    return values


def create_colorbar(colormap):
    colorbar = ColorBar(index_mapper=LinearMapper(range=colormap.range),
                        color_mapper=colormap,
                        orientation='v',
                        resizable='v',
                        width=30,
                        padding=20)
    return colorbar

if __name__ == '__main__':

    data = DataFrame(
        [[23., 'male', 2345., 'ayant-droits', 34.],
         [54., 'female', 122., 'locataire', 45.],
         [45., 'male', 2000., 'ayant-droits', 3.],
         [34., 'female', 1987., 'proprietaire', 23],
         [35., 'female', 2345., 'locataire', 45],
         [22., 'male', 2001., 'locataire', 567.],
         [24., 'female', 145., 'locataire', 10.]
        ],
        columns=['age', 'gender', 'salary', 'status', 'surface']
    )

    viewer = Viewer(dataframe=data, x_dim='age', y_dim='salary', color_dim='surface')
    viewer.configure_traits()
