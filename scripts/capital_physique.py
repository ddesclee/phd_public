# -*- coding: utf-8 -*-
""" Extract physical capital from database. """
import os
from pandas import DataFrame
import numpy as np

from pandas import concat
from pandas.io.excel import ExcelWriter


from sqlalchemy.sql import select, and_, column, func, literal_column, outerjoin
from sqlalchemy.sql.expression import Label

from afdm import afdm, compute_index, compute_doriane_index
from db_utilities import DbConnection
from db_spatial import load_reference, export_to_shapefile
from db_spatial import add_village_name
from plotting import scatter_matrix
from phd_config import EXPORT_DIRECTORY


from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d

def load_physical_capital_from_database():
    
    db = DbConnection()
    
    ### Load table definition
    menage_capital_physique = db.get_table('Menage_CapitalPhysique')
    maison = db.get_table('Menage_CapitalFinancier_Maison')
    equipements = db.get_table('Menage_CapitalFinancier_Equipements')
    materiels = db.get_table('Menage_CapitalFinancier_Materiels')
    enquetes = db.get_table('Enquetes')

    ### Create query
    
    # Expression on basic needs access
  
    #education_access_expression = Label('education_access', 
        #column('EducationPrimaire') + column('EducationSecondaire'),
    #)
    #markets_access_expression = Label('markets_access',
        #2* column('MarcheProdAgric') + column('MarcheProdForestiers')
    #) 

    def convert_bool(col_name):
        return Label(col_name.lower(), func.if_(column(col_name), True, False))
    
    electricite = convert_bool('Electricite')
    eau = convert_bool('Eau')
    #sante = convert_bool('Sante')
    #credits = convert_bool('Credits')
    routes = convert_bool('Routes')
    vehicule = convert_bool('VehiculeTransport')
    
    joined_table = outerjoin(
        outerjoin(
            outerjoin(
                outerjoin(menage_capital_physique, enquetes), 
                maison), 
            materiels, materiels.c.IDEnquete == enquetes.c.IDEnquete
        ), equipements, equipements.c.IDEnquete == enquetes.c.IDEnquete
    )
       
    requete_access = select(
        columns=[
            Label('IDEnquete', column("Enquetes.IDEnquete", is_literal=True)),
            electricite, eau, #sante, #credits, 
            routes, vehicule, #education_access_expression, 
            #markets_access_expression, 
            column('AppartenanceMaison', is_literal=True), 
            Label('valeur_equipements', func.sum(equipements.c.ValeurTotale)),
            Label('valeur_materiels', func.sum(materiels.c.Cout))
        ], 
     ).select_from(joined_table).group_by(enquetes.c.IDEnquete)     
    
    ### Execute
    result = db.execute(requete_access)
    column_names = [str(col) for col in requete_access.columns] 
    return column_names, result, requete_access
    
def capital_physique():
    
    # Load dataset
    columns, result, rq = load_physical_capital_from_database()
    data = result.fetchall()
    
    # TODO: cleanup dataset
    # TOOD: conversion from str to values
    
    dataframe =  DataFrame.from_records(data, columns=columns, coerce_float=True)
    
    # Set the index using IDEnquete and remove it from the dataframe
    id_enquete_col = dataframe.columns[0]
    dataframe.set_index(id_enquete_col, inplace=True, drop=True)
    
    print dataframe.info()
    print dataframe.describe()

    
    from data_cleansing import column_stat
    
    categorical_data = {
        'AppartenanceMaison': column_stat(dataframe, 'AppartenanceMaison'),
        'electricite': column_stat(dataframe, 'electricite'),
        'eau': column_stat(dataframe, 'eau'),
        #'sante': column_stat(dataframe, 'sante'),
        #'credits': column_stat(dataframe, 'credits'),
        'routes': column_stat(dataframe, 'routes'),
        'vehiculetransport': column_stat(dataframe, 'vehiculetransport')
    }

    print '-' * 80
    
    from pylab import show
   
    scatter_matrix(dataframe, categorical_data)    
    show()     
    
    # processing with PCA or FA in sklearn 

    processed_input, pca = afdm(
        dataframe, categorical_data, remove_nan=True, n_components=4,
        whiten=True, with_varimax=True
    )
    
    print 'PCA components:', pca.components_
    print 'PCA explained variance:', pca.explained_variance_
    print 'PCA explained variance ratio:', pca.explained_variance_ratio_
        
    res = pca.transform(processed_input)   

    from sklearn.cluster import KMeans

    ### See http://scikit-learn.org/stable/modules/clustering.html
    estimator = KMeans(init='k-means++', n_clusters=3)
    estimator.fit(res)
    
    
    plt.figure()
    plt.scatter(res[:,0], res[:,1], c=estimator.labels_, s=30)
    plt.gray()
    #plt.title(u'Resultat clustering (3) pour le capital physique')
    plt.xlabel('Composant 0')
    plt.ylabel('Composant 1')
    plt.show()
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(res[:, 0], res[:, 1], res[:, 2], c=estimator.labels_, cmap='jet')
    plt.jet()
    ax.set_xlabel('Composante 0')
    ax.set_ylabel('Composante 1')
    ax.set_zlabel('Composante 2')
    plt.show()
    
     ### Export results
    
    export_pca = DataFrame(pca.components_.T, index=processed_input.columns)
    export_pca_info = DataFrame(
        [pca.explained_variance_, pca.explained_variance_ratio_],
        index=['Explained variance', 'Explained variance ratio']
    )
    
    export_stats = dataframe.describe()

    index = compute_index(processed_input, pca)
    do_index = compute_doriane_index(processed_input, [-1, 1, 1, -1], pca)

    output = DataFrame(
        res,
        columns=['PCA-{}'.format(i) for i in range(pca.components_.shape[0])],
        index=processed_input.index
    )
    output['capital_physique'] = index
    output['do_index_physique'] = do_index
    output = output.join(dataframe)
    
    df_index = DataFrame(do_index, columns=['do_index_physique'], index=processed_input.index)
    
    output_report = output.copy()
    add_village_name(output_report)
    
    export_stats = dataframe.describe()

    village_report = output_report.groupby(by='village')


    fig = plt.figure()
    plt.plot(np.arange(len(index)), index, 'r.')
    plt.title(u'Indicateur de Capital physique par ménage')

    plt.show()
    
    target_file = os.path.join(EXPORT_DIRECTORY, 'capital_physique.xlsx')
    with ExcelWriter(target_file) as writer:
        export_pca.to_excel(writer, 'pca', header=True, index=True)
        export_pca_info.to_excel(writer, 'pca_info', header=True)
        export_stats.to_excel(writer, 'Statistics', header=True)
        output.to_excel(writer, 'Output', header=True)
        village_report.describe().to_excel(writer, 'Stat by village')

    ### Export to shapefile
    target_file = os.path.join(
        EXPORT_DIRECTORY, 'shapefiles', 'capital_physique_inputs.shp'
    )
    ref = load_reference()
    data_with_ref = concat([dataframe, ref], axis=1, join='inner')
    grouped_data = data_with_ref.groupby('IDVillage')
    export_to_shapefile(grouped_data.mean(), target_file)
        
    return dataframe, df_index
        
if __name__ == '__main__':
    capital_physique()