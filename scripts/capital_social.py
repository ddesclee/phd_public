# -*- coding: utf-8 -*-
""" Extract social capital from database. """

import os
from pandas import DataFrame, concat
import numpy as np

from pandas.io.excel import ExcelWriter

from sqlalchemy.sql import select, and_, column, func, literal_column

from afdm import afdm, compute_index, compute_doriane_index
from db_utilities import DbConnection
from db_spatial import load_reference, export_to_shapefile
from db_spatial import add_village_name
from plotting import scatter_matrix

from phd_config import EXPORT_DIRECTORY

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d

def load_social_capital_from_database():
    
    db = DbConnection()
    
    ### Load table definition
     
    identification_chef = db.get_table('Menage_IdentificationChef2')
    menage_capital_social = db.get_table('Menage_CapitalSocial')
    enquetes = db.get_table('Enquetes')

    ### Create query
    
    ### Aggregation of the social capital
    social_connection_expression = (
        column('CLD') + column('Ayants-droits') + 
        column('EchangeSavoirFaire&Biens') + column('ProjetPAPASA') + 
        func.if_(column('Autre').is_(None), 0, 1)
    )
    
    s = select(
        columns=[
            literal_column('Enquetes.IDEnquete'),
            literal_column('SexeChef'),
            literal_column('AgeChefCorrige'),
            literal_column('EtatMatrimonialChef'),
            #literal_column('AncienneteChef'),
         social_connection_expression
        ], 
        from_obj=[identification_chef, enquetes, menage_capital_social],
        whereclause=and_(
            identification_chef.c.IDEnquete == enquetes.c.IDEnquete,
            menage_capital_social.c.IDEnquete == enquetes.c.IDEnquete
        )
    )
    
    ### Execute
    result = db.execute(s)
    columns = (
        'íd_enquete', 'sexe_chef', 'AgeChefCorrige', 'etatmatrimonialchef', #'anciennete_chef',
        'social_connection_count'
    ) 
    return columns, result
    
def capital_social():
    
    # Load dataset
    columns, result = load_social_capital_from_database()
    data = result.fetchall()
    
    # TODO: cleanup dataset
    # TOOD: conversion from str to values
    
    dataframe = DataFrame.from_records(data, columns=columns, coerce_float=True)
    # Set the index using IDEnquete and remove it from the dataframe
    id_enquete_col = dataframe.columns[0]
    dataframe.set_index(id_enquete_col, inplace=True, drop=True)

    print dataframe.info()
    print dataframe.describe()

    print '-' * 80
    print 'Converting str col to unique integers'
    
    from data_cleansing import column_stat
    
    categorical_data = {
        'sexe_chef': column_stat(dataframe, 'sexe_chef'),
        'etatmatrimonialchef': column_stat(dataframe, 'etatmatrimonialchef'),
    }
    print '-' * 80

    from pylab import show
    
    scatter_matrix(dataframe, categorical_data)
    show() 
    
    # processing with PCA or FA in sklearn
    
    processed_input, pca = afdm(
        dataframe, categorical_data, remove_nan=True, n_components=3,
        whiten=True, with_varimax=True
    )
    
    print 'PCA components:', pca.components_
    print 'PCA explained variance:', pca.explained_variance_
    print 'PCA explained variance ratio:', pca.explained_variance_ratio_
        
    res = pca.transform(processed_input.values)
    
    print '-'* 50
    print '-'* 50
    print dataframe[:2]
    print '-'* 50
    print processed_input[:2]
    print processed_input.describe()
    print '-'* 50
    print res[:2]
    print '-'* 50    
    
    from sklearn.cluster import KMeans

    ### See http://scikit-learn.org/stable/modules/clustering.html
    estimator = KMeans(init='k-means++', n_clusters=1)
    estimator.fit(res)
    
    plt.figure()
    plt.scatter(res[:,0], res[:,1], c=estimator.labels_, s=30)
    plt.gray()
    #plt.title(u'Resultat clustering (3) des ménages pour le capital social')
    plt.xlabel('Composant 0')
    plt.ylabel('Composant 1')
    plt.show()
    
     ### Export results
    
    export_pca = DataFrame(pca.components_.T, index=processed_input.columns)
    export_pca_info = DataFrame(
        [pca.explained_variance_, pca.explained_variance_ratio_],
        index=['Explained variance', 'Explained variance ratio']
    )
    
    export_stats = dataframe.describe()

    index = compute_index(processed_input, pca)
    do_index = compute_doriane_index(processed_input, [1, -1, 1], pca)

    output = DataFrame(res, columns=['PCA-0', 'PCA-1', 'PCA-2'], index=processed_input.index)
    output['capital_social'] = index
    output['do_index_social'] = do_index
    output = output.join(dataframe)
    
    df_index = DataFrame(do_index, columns=['do_index_social'], index=processed_input.index)

        
    # Verify that the results are the 'expected' ones
    #if not np.allclose(index.max(), 2.15322083681):
    #    raise RuntimeError('Capital value is not the exected one')
        
    output_report = output.copy()
    add_village_name(output_report)
    
    village_report = output_report.groupby(by='village')


    fig = plt.figure()
    plt.plot(np.arange(len(index)), index, 'r.')
    plt.title(u'Indicateur de Capital social par ménage')

    plt.show()
    
    target_file = os.path.join(EXPORT_DIRECTORY, 'capital_social.xlsx')
    with ExcelWriter(target_file) as writer:
        export_pca.to_excel(writer, 'pca', header=True, index=True)
        export_pca_info.to_excel(writer, 'pca_info', header=True)
        export_stats.to_excel(writer, 'Statistics', header=True)
        output.to_excel(writer, 'Output', header=True)
        village_report.describe().to_excel(writer, 'Stat by village')
        
    ### Export to shapefile
    target_file = os.path.join(
        EXPORT_DIRECTORY, 'shapefiles', 'capital_social_inputs.shp'
    )
    ref = load_reference()
    data_with_ref = concat([dataframe, ref], axis=1, join='inner')
    grouped_data = data_with_ref.groupby('IDVillage')
    export_to_shapefile(grouped_data.mean(), target_file)

    return dataframe, df_index
        
if __name__ == '__main__':
    df, output = capital_social()