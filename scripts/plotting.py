from pandas.tools import plotting
        
def wrap(txt, width=6):
    import textwrap
    return '\n'.join(textwrap.wrap(txt, width))

def scatter_matrix(dataframe, categorical_data=None):
    
    if categorical_data is not None:        
        # show scatter matrix only on continuous columns
        continuous_col = [
            col for col in dataframe.columns if col not in categorical_data
        ]
        data = dataframe[continuous_col]
    else:
        data = dataframe
        
    axs = plotting.scatter_matrix(data, diagonal='kde');
    
    for ax in axs[:,0]:
        ax.grid('off', axis='both')
        ax.set_ylabel(wrap(ax.get_ylabel()), rotation=0, va='center', labelpad=30)
        ax.set_yticks([])
        
    for ax in axs[-1,:]:
        ax.grid('off', axis='both')
        ax.set_xlabel(wrap(ax.get_xlabel()), rotation=0, va='center', labelpad=30)
        ax.set_xticks([])