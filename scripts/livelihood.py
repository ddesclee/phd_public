""" Livelihood estimation """
# Python standard library imports
import os

# 3rd party imports
import matplotlib.pyplot as plt
from pandas import concat
from pandas.io.excel import ExcelWriter

# Local imports
from capital_naturel import capital_naturel
from capital_physique import capital_physique
from capital_financier import capital_financier
from capital_humain import capital_humain
from capital_social import capital_social
from db_spatial import load_reference, export_to_shapefile
from phd_config import EXPORT_DIRECTORY

def compute_livelihood():
    data, cnat = capital_naturel()
    data, cphys = capital_physique()
    data, cfin = capital_financier()
    data, chum = capital_humain()
    data, csoc = capital_social()
    
    plt.close('all')
    
    livelihood = concat([cnat, cphys, cfin, chum, csoc], axis=1)
    
    # if we want to drop missing values
    livelihood.dropna(inplace=True)
            
    reference = load_reference()
    dataframe_with_reference = concat((livelihood, reference), axis=1, join='inner')
    grouped = dataframe_with_reference.groupby('IDVillage')
    
    median_per_village = grouped.median()
    
    output = median_per_village.copy()
    #add_village_name(output)
    
    output['index_naturel'] = output['do_index_naturel']
    output['index_physique'] = output['do_index_physique']
    output['index_financier'] = output['do_index_financier']
    output['index_humain'] = output['do_index_humain']
    output['index_social'] =  output['do_index_social']
    
    output['livelihood'] = (
        output['do_index_naturel']  + output['do_index_physique'] +
        output['do_index_financier'] + output['do_index_humain'] +
        output['do_index_social']
    )     
                           
    target_file = os.path.join(EXPORT_DIRECTORY, 'livelihood.xlsx')
    with ExcelWriter(target_file) as writer:
        livelihood.to_excel(writer, 'livelihood', header=True, index=True)
        output.to_excel(writer, 'village_aggregation')
        
    ### Export to shapefile
    target_file = os.path.join(EXPORT_DIRECTORY, 'shapefiles', 'livelihood_inputs.shp')
    export_to_shapefile(output, target_file)
    
    return livelihood, output
            
if __name__ == '__main__':
    o = compute_livelihood()