# -*- coding: utf-8 -*-
""" Extract human capital from database. """
import os
import numpy as np
from pandas import DataFrame, concat
from pandas.io.excel import ExcelWriter

from sqlalchemy.sql import select, and_, column, func
from sqlalchemy.sql.expression import Label

from afdm import afdm, compute_index, compute_doriane_index
from db_utilities import DbConnection
from db_spatial import load_reference, export_to_shapefile
from db_spatial import add_village_name
from plotting import scatter_matrix
from phd_config import EXPORT_DIRECTORY

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d

def load_human_capital_from_database():
    
    db = DbConnection()
    
    ### Load table definition
     
    identification_chef =db.get_table('Menage_IdentificationChef')
    menage_capital_physique = db.get_table('Menage_CapitalPhysique')
    menage_capital_humain = db.get_table('Menage_CapitalHumain')
    menage_capital_humain_activites = db.get_table('Menage_Revenus_SourcesDirectes2')
    menage_composition = db.get_table('Menage_Composition')
    menage_membres = db.get_table('Menage_Membres')
    enquetes = db.get_table('Enquetes')

    ### Create query
    
    # Expression on menage_membres
    MembresSup12ans_expression = Label('MembresSup12',
        func.sum(func.if_(column('AnneeNaissance') <= (2001), 1, 0))
    )
    MoyenneAgeMenage_expression = Label(
        'MoyenneAge', func.avg(2013 - column('AnneeNaissance'))   
    )
    
    #education_access_expression = Label('education_access', 
        #column('EducationPrimaire') + column('EducationSecondaire'),
    #)
        
    # Expressions on menage_capital_humain
    #SanteMenage_expression = Label(
        #'MaxSante', func.floor(func.max(column('NiveauSante')))
    #)
   # EducationMenage_expression = Label(
        #'MaxEducation', func.floor(func.max(column('NiveauEducation')))
    #)
    
    # Exprerssion on ratio males/females
    GenderRatio_expression = Label('GenderRatio', (column('NombreHommes') / column('NombreFemmes'))
    )
    
    def convert_bool(col_name):
        return Label(col_name.lower(), func.if_(column(col_name), True, False))
    
    sante = convert_bool('Sante')
   
    ### Aggregation of the human capital
     
    select_human_capital = select(
        columns=[
            '`Enquetes`.`IDEnquete`', #'SexeChef', 
            #'AgeChefCorrige', 
            'NiveauInsructionChef',
            'NombreMembres', 
            'NiveauEducation',
            #'NombreHommes', 
            #'NombreFemmes', 
            #SanteMenage_expression,
            #EducationMenage_expression, 
            #education_access_expression, 
            #sante, 
            GenderRatio_expression,
            #MembresSup12ans_expression,
            MoyenneAgeMenage_expression,
            'ActivitePrincipale',#'ActiviteSecondaire'
        ], 
        from_obj=[
            identification_chef, menage_composition, menage_capital_humain, 
            menage_membres, menage_capital_humain_activites, menage_capital_physique, enquetes
        ],
        whereclause=and_(
            menage_capital_humain.c.IDEnquete == enquetes.c.IDEnquete,
            menage_membres.c.IDEnquete == menage_capital_humain.c.IDEnquete,
            menage_membres.c.IDMembre == menage_capital_humain.c.IDMembre,
            identification_chef.c.IDEnquete == enquetes.c.IDEnquete,
            menage_composition.c.IDEnquete == enquetes.c.IDEnquete,
            menage_capital_humain_activites.c.IDEnquete == enquetes.c.IDEnquete
        )
    ).group_by(enquetes.c.IDEnquete)

    ### Execute
    result = db.execute(select_human_capital)
    column_name = [col.name for col in select_human_capital.columns] 
    return column_name, result
    
def capital_humain():
    
    # Load dataset
    columns, result = load_human_capital_from_database()
    data = result.fetchall()
    
    dataframe =  DataFrame.from_records(data, columns=columns, coerce_float=True)
    
    # Set the index using IDEnquete and remove it from the dataframe
    id_enquete_col = dataframe.columns[0]
    dataframe.set_index(id_enquete_col, inplace=True, drop=True)
    
    print dataframe.info()    
    print dataframe.describe()
    
   
    # Convert str columns to unique integers
    
    print '-' * 80
    print 'Converting str col to unique integers'
    
    from data_cleansing import column_stat
    
    categorical_data = {
        #'SexeChef': column_stat(dataframe, 'SexeChef'),
        'NiveauInsructionChef': column_stat(dataframe, 'NiveauInsructionChef'),
        'ActivitePrincipale': column_stat(dataframe, 'ActivitePrincipale'),
        'NiveauEducation' : column_stat(dataframe, 'NiveauEducation'),
        #'sante': column_stat(dataframe, 'sante'),
        #'education_access': column_stat(dataframe, 'education_access'),
        #'ActiviteSecondaire': column_stat(dataframe, 'ActiviteSecondaire'),
        #'MaxSante': column_stat(dataframe, 'MaxSante'),
        #'MaxEducation': column_stat(dataframe, 'MaxEducation')
    }
    print
    print '-' * 80
   
    scatter_matrix(dataframe, categorical_data)
        
    plt.show()     
    
    # processing with PCA or FA in sklearn
        
    processed_input, pca = afdm(
        dataframe, categorical_data, remove_nan=True, n_components=3,
        whiten=True, with_varimax=True
    )
        
    print 'Processed inputs:'
    print processed_input.info()
    
    print 'PCA components:', pca.components_
    print 'PCA explained variance:', pca.explained_variance_
    print 'PCA explained variance ratio:', pca.explained_variance_ratio_
    
    res = pca.transform(processed_input)   

    from sklearn.cluster import KMeans

    ### See http://scikit-learn.org/stable/modules/clustering.html
    estimator = KMeans(init='k-means++', n_clusters=3)
    estimator.fit(res)
    
    plt.figure()
    plt.scatter(res[:,0], res[:,1], c=estimator.labels_, s=500)
    plt.gray()
    plt.title(u'Resultat clustering pour le capital humain')
    plt.xlabel('Composant 0')
    plt.ylabel('Composant 1')
    plt.show()
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(res[:, 0], res[:,1], res[:,2], c=estimator.labels_, cmap='jet')
    plt.jet()
    plt.title(u'Résultats de l''AFDM en 3D pour le capital humain')
    ax.set_xlabel('Composante 0')
    ax.set_ylabel('Composante 1')
    ax.set_zlabel('Composante 2')
    plt.show()
    
    
    #import viewer    
    #v = viewer.Viewer(dataframe, x_dim='NombreMembres', y_dim='NombreHommes', color_dim='cluster')
    #v.configure_traits()
        
    ### Export results
    
    export_pca = DataFrame(pca.components_.T, index=processed_input.columns)
    export_pca_info = DataFrame(
        [pca.explained_variance_, pca.explained_variance_ratio_],
        index=['Explained variance', 'Explained variance ratio']
    )
      
    export_stats = dataframe.describe()
    
    index = compute_index(processed_input, pca)
    do_index = compute_doriane_index(processed_input, [1, -1, -1], pca)

    output = DataFrame(
        res,
        columns=['PCA-{}'.format(i) for i in range(pca.components_.shape[0])],
        index=processed_input.index
    )
    output['capital_humain'] = index
    output['do_index_humain'] = do_index
    output = output.join(dataframe)
    
    df_index = DataFrame(do_index, columns=['do_index_humain'], index=processed_input.index)

    
    output_report = output.copy()
    add_village_name(output_report)
    village_report = output_report.groupby(by='village')

    fig = plt.figure()
    plt.plot(np.arange(len(index)), index, 'r.')
    plt.title(u'Indicateur de Capital humain par ménage')

    plt.show()
    
    ### Export to Excel
    target_file = os.path.join(EXPORT_DIRECTORY, 'capital_humain.xlsx')
    with ExcelWriter(target_file) as writer:
        export_pca.to_excel(writer, 'pca', header=True, index=True)
        export_pca_info.to_excel(writer, 'pca_info', header=True)
        export_stats.to_excel(writer, 'Statistics', header=True)
        output.to_excel(writer, 'Output', header=True)
        village_report.describe().to_excel(writer, 'Stat by village')
        
    ### Export to shapefile
    target_file = os.path.join(EXPORT_DIRECTORY, 'shapefiles', 'capital_humain_inputs.shp')
    ref = load_reference()
    data_with_ref = concat([dataframe, ref], axis=1, join='inner')
    grouped_data = data_with_ref.groupby('IDVillage')
    export_to_shapefile(grouped_data.mean(), target_file)

    ### See http://scikit-learn.org/stable/modules/clustering.html
    estimator = KMeans(init='k-means++', n_clusters=3)
    estimator.fit(index.reshape(-1, 1))

    
    plt.figure()
    plt.scatter(processed_input.index, index, c=estimator.labels_, s=30)
    plt.gray()
    #plt.title(u'Resultat clustering pour le capital humain')
    plt.xlabel(u'Id Ménage')
    plt.ylabel(u'Valeur Index Capital Humain')
    plt.show()
    
    return dataframe, df_index

if __name__ == '__main__':
    raw_data, output = capital_humain()