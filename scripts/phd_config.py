""" Common utilities for PhD data, results, etc. """
import os
from pandas import options


options.io.excel.xlsx.writer = 'openpyxl'


EXPORT_DIRECTORY = os.path.join(
    os.path.expanduser('~'), 'Desktop', 'PhD', 'source', 'livelihood_data_export'
)

EXCEL_PCA = os.path.join(EXPORT_DIRECTORY, 'pca.xlsx')

