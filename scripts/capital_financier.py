# -*- coding: utf-8 -*-
""" Extract financial capital from database. """
import os
import numpy as np
from pandas import DataFrame, concat
from pandas.io.excel import ExcelWriter
from pylab import show

from sqlalchemy.sql import select, outerjoin, func
from sqlalchemy.sql.expression import Label

from afdm import afdm, compute_index, compute_doriane_index
from db_utilities import DbConnection, zero_replace_null
from db_spatial import add_village_name, load_reference, export_to_shapefile
from plotting import scatter_matrix
from phd_config import EXPORT_DIRECTORY

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d

def load_financial_capital_from_database():
    
    db = DbConnection()
    
    ### Load table definition
     
    menage_revenu_direct = db.get_table('Menage_Revenus_SourcesDirectes222')
    credits = db.get_table('Menage_Credits')
    dettes_epargne = db.get_table('Menage_CapitalFinancier_Epargne')
    autoconsommation = db.get_table('Menage_Revenus_Directs&Autoconsommation')
    transferts = db.get_table('Menage_Revenus_Transferts')
    enquetes = db.get_table('Enquetes')

    ### Create query
    
    # Expression on revenu
  
    revenu_direct_expression = Label(
        'revenu_direct', zero_replace_null('RevenuNetCorrige')
    )
    
    # Expression on credits
    
    credits_expression = Label('credits',
        zero_replace_null('MontantCreditaTiers') - zero_replace_null('MontantCreditContracte'),
    )
    
    # Expression on Saving

    saving_expression = Label('epargne',
        zero_replace_null('ValeurEpargne') - zero_replace_null('ValeurDette'),
    )
    
    # Expression on main expenditures
    
    #main_expenditures_expression = Label('depenses_principales',
        #zero_replace_null('DepensesAlimentaires') + zero_replace_null('DepensesSante') + 
        #zero_replace_null('DepensesScolarite') + zero_replace_null('DepensesAutres'),
    #)    

    
    # Expression on autoconsumption
    
    autoconsumption_expression = Label(
        'valeur_autoconsommation', zero_replace_null('ValeurBrute') - zero_replace_null('RevenuVente'),
    ) 
        
    # Expression on transfers
    transferts_expression = Label('transferts', func.sum(zero_replace_null('Montant')))
   
    joined_table = outerjoin(
        outerjoin(
            outerjoin(
                outerjoin(outerjoin(menage_revenu_direct, enquetes), credits),
                dettes_epargne
            ), 
            autoconsommation,
            enquetes.c.IDEnquete == autoconsommation.c.IDEnquete
        ), transferts,
        enquetes.c.IDEnquete == transferts.c.IDEnquete
    )


    requete_capital_financier = select(
        columns=[
            enquetes.c.IDEnquete, revenu_direct_expression, 
            credits_expression, saving_expression, 
            #main_expenditures_expression, 
            autoconsumption_expression, transferts_expression, 
        ],
    ).select_from(joined_table).where(menage_revenu_direct.c.RevenuNetCorrige > 0).group_by(enquetes.c.IDEnquete)  
    
    ### Execute
    result = db.execute(requete_capital_financier)
       
    column_name = [str(col) for col in requete_capital_financier.columns] 
    
    return column_name, result, requete_capital_financier
    
def capital_financier():
    
    # Load dataset
    columns, result, rq = load_financial_capital_from_database()
    data = result.fetchall()
    
    dataframe = DataFrame.from_records(data, columns=columns, coerce_float=True)
    # Set the index using IDEnquete and remove it from the dataframe
    id_enquete_col = dataframe.columns[0]
    dataframe.set_index(id_enquete_col, inplace=True, drop=True)
    
    print dataframe.info()
    
    print dataframe.describe()
   
    scatter_matrix(dataframe)
    show() 
    
    # processing with PCA or FA in sklearn
        
    processed_input, pca = afdm(
        dataframe, {}, remove_nan=True, n_components=4,
        whiten=True, with_varimax=True
    )
    
    print 'PCA components:', pca.components_
    print 'PCA explained variance:', pca.explained_variance_
    print 'PCA explained variance ratio:', pca.explained_variance_ratio_
    
    res = pca.transform(processed_input)
    
    from sklearn.cluster import KMeans

    ### See http://scikit-learn.org/stable/modules/clustering.html
    estimator = KMeans(init='k-means++', n_clusters=4)
    estimator.fit(res)

    
    plt.figure()
    plt.scatter(res[:,0], res[:,1], c=estimator.labels_, s=30)
    plt.gray()
    #plt.title(u'Resultat clustering pour le capital financier')
    plt.xlabel('Composant 0')
    plt.ylabel('Composant 1')
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #ax.scatter(res[:, 0], res[:,1], res[:,2], c=estimator.labels_, cmap='jet')
    plt.jet()
    plt.title(u'Résultats de l''AFDM en 3D pour le capital financier')
    ax.set_xlabel('Composante 0')
    ax.set_ylabel('Composante 1')
    ax.set_zlabel('Composante 2')
    plt.show()
    
    ### Export results
    
    export_pca = DataFrame(pca.components_.T, index=dataframe.columns)
    export_pca_info = DataFrame(
        [pca.explained_variance_, pca.explained_variance_ratio_],
        index=['Explained variance', 'Explained variance ratio']
    )
    
    export_stats = dataframe.describe()

    index = compute_index(processed_input, pca)
    do_index = compute_doriane_index(processed_input, [-1, -1, 1, -1], pca)

    output = DataFrame(
        res,
        columns=['PCA-{}'.format(i) for i in range(pca.components_.shape[0])],
        index=processed_input.index
    )
    output['capital_financier'] = index
    output['do_index_financier'] = do_index
    output = output.join(dataframe)
    
    df_index = DataFrame(do_index, columns=['do_index_financier'], index=processed_input.index)
    
    output_report = output.copy()
    add_village_name(output_report)
    
    export_stats = dataframe.describe()

    village_report = output_report.groupby(by='village')

    fig = plt.figure()
    plt.plot(np.arange(len(index)), index, 'r.')
    plt.title(u'Indicateur de Capital financier par ménage')
    plt.show()
    
    target_file = os.path.join(EXPORT_DIRECTORY, 'capital_financier.xlsx')
    with ExcelWriter(target_file) as writer:
        export_pca.to_excel(writer, 'pca', header=True, index=True)
        export_pca_info.to_excel(writer, 'pca_info', header=True)
        export_stats.to_excel(writer, 'Statistics', header=True)
        output.to_excel(writer, 'Output', header=True)
        village_report.describe().to_excel(writer, 'Stat by village')
        
    ### Export to shapefile
    target_file = os.path.join(
        EXPORT_DIRECTORY, 'shapefiles', 'capital_financier_inputs.shp'
    )
    ref = load_reference()
    data_with_ref = concat([dataframe, ref], axis=1, join='inner')
    grouped_data = data_with_ref.groupby('IDVillage')
    export_to_shapefile(grouped_data.mean(), target_file)

    return dataframe, df_index
        
if __name__ == '__main__':
    output = capital_financier()