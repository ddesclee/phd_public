""" Data cleansing functions. """
import itertools    

def column_stat(dataframe, column):
    return dataframe.groupby(column)[column].count().to_dict()

def convert_str_to_unique_integers(dataframe, column, start_at=0,
                                   col_suffix='_unique'):
    
    unique_values = dataframe[column].unique()
    
    counter = itertools.count(start_at)
    mapping = { key:counter.next() for key in unique_values}
    
    new_column = column + col_suffix
    
    
    print 'Converting %s to %s (%d unique values : %s' % (
            column, new_column, len(unique_values), str(unique_values)
    )
    values = dataframe[column]
    new_values = [mapping[value] for value in values]
    dataframe[new_column] = new_values
    
    return dataframe
    
    dataframe.describe
    
    
    