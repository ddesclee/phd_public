""" AFDM implementation based on

Pages J. (2004). Analyse factorielle de donnees mixtes.
Revue Statistique Appliquee. LII (4). pp. 93-111.

"""
import numpy as np
from numpy.linalg import svd
import pandas
import sklearn.preprocessing
import sklearn.decomposition
from sklearn.preprocessing import MinMaxScaler


def convert_categorical_data(input_data, categories):
    """Converts the input array. Each unique value in the categories dictionary
    is replaced by its weighted value into the target column. """

    results = {}
    total_count = sum(value for value in categories.values())
    for key, count in categories.items():
        weight = 1.0 * count / total_count
        results[key] = np.where(input_data == key, weight, 0)
    return results


def varimax(phi, gamma=1.0, q=20, tol=1e-6):
    """ Varimax rotation implementation from 
    http://stackoverflow.com/questions/17628589/perform-varimax-rotation-in-python-using-numpy
    
    Validated against the R implementation in module stats, funtion varimax
    
    """
    p, k = phi.shape
    rotation = np.eye(k)
    d = 0
    for i in xrange(q):
        d_old = d
        lambda_ = np.dot(phi, rotation)
        u, s, vh = svd(
            np.dot(
                phi.T, np.asarray(lambda_)**3 - (gamma / p) * np.dot(
                    lambda_, np.diag(np.diag(np.dot(lambda_.T,lambda_)))
                )
            )
        )
        rotation = np.dot(u, vh)
        d = sum(s)
        if d_old != 0 and (d / d_old) < (1. + tol):
            break
            
    return np.dot(phi, rotation)


def afdm(values, categorical_data, remove_nan=False, n_components=None,
         whiten=False, with_varimax=True):
    """ Returns processed input values and a fitted PCA for the given input
    values after:
        - replacing nans by zeros if remove_nan is True (FIXME: remove NaN
           should remove nan's)
        - scale continuous value to unit variance and centered on zero
        - convert categorical values into rescaled inputs.
        
    Processed_inputs is a pandas dataframe using the same index as the input
    values.
        
    """
    if remove_nan is True:
        values = values.dropna(how='any')
    else:
        values = values.copy()
        
    data = values.copy()

    categorical_cols = set(col for col in categorical_data.keys())
    continuous_cols = set(data.columns) - categorical_cols

    for column in categorical_cols:
        del data[column]
        
    values_for_pca = data.values

    # Ensure all the variables are standardized
    # get a scaled copy of the full dataset
    #_data = sklearn.preprocessing.scale(values_for_pca, axis=1, copy=True)
    _data = (values_for_pca - values_for_pca.mean(axis=0)) / values_for_pca.std(axis=0)

    categorical_results = {}
    # process categorical data
    for index, column in enumerate(values.columns):
        if column in categorical_data:
            categories = categorical_data[column]
            c_value = values[column].values
            for key, value in convert_categorical_data(c_value, categories).items():
                categorical_results['{}-{}'.format(column, key)] = value

    processed_input = pandas.DataFrame(
        _data,
        columns=data.columns,
        index=values.index
    )
    for converted_category in categorical_results:
        processed_input[converted_category] = categorical_results[converted_category]

    # run the PCA now
    pca = sklearn.decomposition.PCA(n_components=n_components, whiten=whiten)
    pca.fit(processed_input.values)
    
    if with_varimax is True:
        corrected_components = varimax(pca.components_)
        pca.original_components = pca.components_
        pca.components_ = corrected_components
        
    return processed_input, pca


def compute_index(inputs, pca):
    """Based on the processed inputs, returns the weighted sum of the
    transformed inputs in the pca space. The weights are computed based on the
    explained variance ratio. 
    
    IIRC this is suggested in the AFDM paper.
    
    """
    
    outputs = pca.transform(inputs)   
    variance_ratio = pca.explained_variance_ratio_
    weights =  variance_ratio / variance_ratio.sum()
    index = np.dot(outputs, weights)  
    
    return index

def compute_doriane_index(inputs, signs, pca):
    """ Based on the processed inputs, returns the weighted sum of the
    transformed inputs in the pca space. The weights are computed based on the
    explained variance ratio.
    
    1. The inputs (initial variables) are transformed using the PCA kernel.
    
    2. The outputs are multiplied by the signs to transform the projected inputs
    into a qualitative information (positive effects should be > 0 and negative
    effect should be < 0.
    
    3. Weights are estimated based on the explained variance ratio (IIRC this is
    suggestd in the AFDM paper)
    
    4. An intermediate index is computed as the weighted sum of the
    qualitative_outputs times the weights
    
    5. Rescaling of the intermediate index to an interval starting at zero.
    The intention is to provide an index that cannot be interpreted as negative
    vs positive but just in terms of relative value. 
    
    From [min_index, max_index]  -> [0, max_index + min_index]
            
    """
    
    # Transformation of the inputs
    outputs = pca.transform(inputs)
    qualitative_outputs = outputs * signs  

    # Computing of the weights
    variance_ratio = pca.explained_variance_ratio_
    weights =  variance_ratio / variance_ratio.sum()

    # Index computation
    index = np.dot(qualitative_outputs, weights)  
    
    # Rescaling
    scaler = MinMaxScaler()
    scaled_index = scaler.fit_transform(index)    
    
    min_value = index.min()
    if min_value < 0.0:
        new_zero = np.abs(min_value)
    else:
        new_zero = -min_value
        
    print 'MIN value ', min_value, new_zero, index.argmin(), len(index)      
       
    intermediate = (index + new_zero )
    diff = (intermediate / intermediate.max()) - scaled_index
    
    print diff
      
    return scaled_index
            
def test_convert_categorical_data():

    data = pandas.DataFrame(
        [
            'male',
            'female',
            'male',
            'female',
            'female'
        ],
        columns=['sex']
    )

    results = convert_categorical_data(data, {'male':2, 'female':3})

    assert(len(results) == 2)


def test_afdm():

    data = pandas.DataFrame(
        [[23., 'male', 2345., 'ayant-droits'],
         [54., 'female', 122., 'locataire'],
         [45., 'male', 2000., 'ayant-droits'],
         [34., 'female', 1987., 'proprietaire'],
         [35., 'female', 2345., 'locataire'],
         [22., 'male', 2001., 'locataire'],
         [24., 'female', 145., 'locataire']
        ],
        columns=['age', 'gender', 'salary', 'status']
    )

    processed_data, pca = afdm(data,
        {'gender': {'male':3., 'female':4.},
         'status': {'ayant-droits':2., 'locataire':3., 'proprietaire':3.}
        }
    )

    assert(len(processed_data.columns) == 7)


if __name__ == '__main__':

    test_afdm()

    