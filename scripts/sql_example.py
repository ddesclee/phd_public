""" Exemple de selection de donne dans la db mysql. """

import pymysql
from pandas.io.sql import read_frame

query_string = ("SELECT Villages.IDVillage, Villages.NomVillage, "
               "count(Enquetes.Numero) as cnt from Villages, Enquetes where "
               "Enquetes.IDVillage = Villages.IDVillage group by IDVillage")


def example_with_sql(conn, query):
    
    cur = conn.cursor()
    cur.execute(query)

    print(cur.description)
    
    for row in cur:
        print(row)
    
    cur.close()
    
def example_with_pandas(conn, query):
    frame = read_frame(query_string, conn)
    frame.to_csv('enquetes_stats.csv')
    return frame
    
    
def example_with_sqlalchemy():

    from sqlalchemy import create_engine, Table, MetaData
    
    meta = MetaData()
    engine = create_engine('mysql+pymysql://ddesclee:luki@127.0.0.1:8889/rdc_luki_draft')
    enquetes = Table('Enquetes', meta, autoload=True, autoload_with=engine)
    villages = Table('Villages', meta, autoload=True, autoload_with=engine)
    
    #print [c.name for c in enquetes.columns]

    from sqlalchemy.sql import select
    conn = engine.connect()
    
    s = select([enquetes])
    result = conn.execute(s)
    return result
    
if __name__ == '__main__':
    
    conn = pymysql.connect(host='127.0.0.1', port=8889, user='ddesclee', passwd='luki', db='rdc_luki_draft')
    example_with_pandas(conn, query_string)
    conn.close()
    