# -*- coding: utf-8 -*-
"""Example using fiona. """
import fiona
import pymysql

path = "/Users/ddesclee/Desktop/PhD/Recherche/Data/SIG_Luki/Couches thématiques/Villages.shp"

villages =  fiona.open(path, 'r') 
source_driver = villages.driver
source_crs = villages.crs
source_schema = villages.schema
        

# Count members per village

query_string = ("SELECT Villages.IDVillage, Villages.NomVillage, "
               "count(Menage_Membres.IDMembre) as members_count "
               "from Villages, Enquetes, Menage_Membres where "
               "Enquetes.IDVillage = Villages.IDVillage and "
               "Menage_Membres.IDEnquete = Enquetes.IDEnquete "
               "group by IDVillage")
               
print query_string
conn = pymysql.connect(host='127.0.0.1', port=8889, user='ddesclee', passwd='luki', db='rdc_luki_draft')
cur = conn.cursor()
cur.execute(query_string)

print(cur.description)

members_count_dict = {}

total = 0
for row in cur:
    village_id = row[0]
    members_count = row[2]
    members_count_dict[village_id] = members_count
    total += members_count
    
print members_count_dict
print '-' * 50

# Create a shapefile with villages and adding the members_count column
target_schema = source_schema.copy()
target_schema['properties']['members_co'] = 'int:10'

    

with fiona.open('/Users/ddesclee/Desktop/PhD/temp/villages_updated.shp', 'w', driver=source_driver, crs=source_crs, schema=target_schema) as target_layer:
    for village in villages:
        village_id = int(village['properties']['id'])
        properties = village['properties'].copy()
        try:
            members_count = members_count_dict[village_id]
            properties['members_co'] = int(members_count)
        except KeyError:
            print 'ERROR with ', village
        target_layer.write({
            'properties':properties,
            'geometry': village['geometry'] 
        
        })


print 'TOTAL', total
cur.close()


