# -*- coding: utf-8 -*-
""" Extract natural capital from database. """
import os

import numpy as np
from pandas import DataFrame, concat
from pandas.io.excel import ExcelWriter

from sqlalchemy.sql import select, outerjoin, literal_column
from db_spatial import add_village_name, load_reference, export_to_shapefile
from sqlalchemy.sql.expression import Label

from afdm import afdm, compute_index, compute_doriane_index
from db_utilities import DbConnection, zero_replace_null
from plotting import scatter_matrix
from phd_config import EXPORT_DIRECTORY
from data_cleansing import column_stat

from matplotlib import pyplot as plt
# required for projection 3D to work
from mpl_toolkits.mplot3d import axes3d, Axes3D 

def load_natural_capital_from_database():
    
    db = DbConnection()
    
    ### Load table definition
     
    statut_foncier =  db.get_table('Menage_CapitalNaturel_StatutFoncier')
    terres_cultivees = db.get_table('Menage_CapitalNaturel_TerresAgricoles')
    acces_espaceforestier = db.get_table('Menage_CapitalNaturel_RessourcesForestieres')
    pfl = db.get_table('Menage_CapitalNaturel_RessourcesForestieres_PFL')
    pfnl = db.get_table('Menage_CapitalNaturel_RessourcesForestieres_PFNL')
    enquetes = db.get_table('Enquetes')

    ### Create query
    
    # Expression on land access and use   
    cultures = Label('surface_cultivee',
        zero_replace_null('SuperficieCulturesVivrieres') + 
        zero_replace_null('SuperficieCulturesPerennes') + 
        zero_replace_null('SuperficieCulturesIndustrielles')
    )
    
    # Expression on forest access and use
                                              
    consommation_pfl = Label(
        'conso_pfl', 
        zero_replace_null(pfl.c.ValeurAutoconsommation) + zero_replace_null(pfl.c.ValeurVente) 
        + zero_replace_null(pfl.c.ValeurTableSourcesDirectes)
    )
    
    consommation_pfnl = Label(
        'conso_pfnl', 
        zero_replace_null(pfnl.c.ValeurAutoconsommation) + zero_replace_null(pfnl.c.ValeurVente)
    )
   
    joined_tables = outerjoin(
        outerjoin(
            outerjoin(
                outerjoin(
                    outerjoin(statut_foncier, enquetes), terres_cultivees
                ), acces_espaceforestier
            ), pfl,
            pfl.c.IDEnquete == enquetes.c.IDEnquete
        ), pfnl,
        pfnl.c.IDEnquete == enquetes.c.IDEnquete
    )
   
    requete_capital_naturel = select(
        columns=[
            literal_column('Enquetes.IDEnquete'), 
            literal_column('StatutFoncier'), 
            cultures, 
            #literal_column('RevenuAnnuelCulturesVivrieres'), 
            #literal_column('RevenuAnnuelCulturesPerennes'), 
            #literal_column('RevenuAnnuelCulturesIndustrielles'), 
            literal_column('AccesEspacesForestiers'), 
            consommation_pfl, consommation_pfnl,
        ],
    ).select_from(joined_tables).group_by(enquetes.c.IDEnquete)
      
    ### Execute
    result = db.execute(requete_capital_naturel)
    column_name = [str(col) for col in requete_capital_naturel.columns] 
    return column_name, result, requete_capital_naturel

    
def capital_naturel():
    
    # Load dataset
    columns, result, rq = load_natural_capital_from_database()
    data = result.fetchall()
    
    dataframe = DataFrame.from_records(data, columns=columns, coerce_float=True)
    
    # Set the index using IDEnquete and remove it from the dataframe
    id_enquete_col = dataframe.columns[0]
    dataframe.set_index(id_enquete_col, inplace=True, drop=True)

    print dataframe.info()
    print dataframe.describe()

    categorical_data = {
        'StatutFoncier': column_stat(dataframe, 'StatutFoncier'),
        'AccesEspacesForestiers': column_stat(dataframe, 'AccesEspacesForestiers')
    }
    
    print '-' * 80

    # Validation des productions totales de cultures perennes.
    #valid_prod = invert(dataframe.ProductionTotalCulturesPerennes >= 1648000.0)
    #plot(dataframe.surface_cultivee[valid_prod], dataframe.ProductionTotalCulturesPerennes[valid_prod], 'r*')
    
    scatter_matrix(dataframe, categorical_data)
    plt.show()
    
    for column_name in [#'RevenuAnnuelCulturesVivrieres',
                   #'RevenuAnnuelCulturesPerennes', 
                   #'RevenuAnnuelCulturesIndustrielles',
                   'conso_pfl',
                   'conso_pfnl',
                   ]: 
        column = dataframe[column_name]
        column[column.isnull()] = 0 
    
    # processing with PCA or FA in sklearn
            
    print 'PCA'

    processed_input, pca = afdm(
        dataframe, categorical_data, remove_nan=True, n_components=3,
        whiten=True, with_varimax=True
    )
    
    print 'PCA explained variance:', pca.explained_variance_
    print 'PCA explained variance ratio:', pca.explained_variance_ratio_
    
    res = pca.transform(processed_input.values)
    
    ### Export results
    
    export_pca = DataFrame(pca.components_.T, index=processed_input.columns)
    export_pca_info = DataFrame(
        [pca.explained_variance_, pca.explained_variance_ratio_],
        index=['Explained variance', 'Explained variance ratio']
    )
  
    export_stats = dataframe.describe()

    index = compute_index(processed_input, pca)
    do_index = compute_doriane_index(processed_input, [1, 1, 1], pca)

    output = DataFrame(
        res,
        columns=['PCA-{}'.format(i) for i in range(pca.components_.shape[0])],
        index=processed_input.index
    )
    output['capital_naturel'] = index
    output['do_index_naturel'] = do_index
    output = output.join(dataframe)

    df_index = DataFrame(do_index, columns=['do_index_naturel'], index=processed_input.index)

    output_report = output.copy()
    add_village_name(output_report)
    
    village_report = output_report.groupby(by='village')
    
    fig = plt.figure()
    plt.plot(np.arange(len(index)), index, 'r.')
    plt.show()
    
    target_file = os.path.join(EXPORT_DIRECTORY, 'capital_naturel.xlsx')
    with ExcelWriter(target_file) as writer:
        export_pca.to_excel(writer, 'pca', header=True, index=True)
        export_pca_info.to_excel(writer, 'pca_info', header=True)
        export_stats.to_excel(writer, 'Statistics', header=True)
        output.to_excel(writer, 'Output', header=True)
        village_report.describe().to_excel(writer, 'Stat by village')
        
    ### Export to shapefile
    target_file = os.path.join(
        EXPORT_DIRECTORY, 'shapefiles', 'capital_naturel_inputs.shp'
    )
    ref = load_reference()
    data_with_ref = concat([dataframe, ref], axis=1, join='inner')
    grouped_data = data_with_ref.groupby('IDVillage')
    export_to_shapefile(grouped_data.mean(), target_file)

    return dataframe, df_index
    
if __name__ == '__main__':
    capital_naturel()