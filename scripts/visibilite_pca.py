
from sklearn.decomposition import PCA

from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

# Plot the figures
def plot_figs(fig_num, elev, azim):
    fig = plt.figure(fig_num, figsize=(4, 3))
    plt.clf()
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=elev, azim=azim)

    ax.scatter(a[::10], b[::10], c[::10], c=density, marker='+', alpha=.4)
    Y = np.c_[a, b, c]

    # Using SciPy's SVD, this would be:
    # _, pca_score, V = scipy.linalg.svd(Y, full_matrices=False)

    pca = PCA(n_components=3)
    pca.fit(Y)
    pca_score = pca.explained_variance_ratio_
    V = pca.components_

    x_pca_axis, y_pca_axis, z_pca_axis = V.T * pca_score / pca_score.min()

    x_pca_axis, y_pca_axis, z_pca_axis = 3 * V.T
    x_pca_plane = np.r_[x_pca_axis[:2], - x_pca_axis[1::-1]]
    y_pca_plane = np.r_[y_pca_axis[:2], - y_pca_axis[1::-1]]
    z_pca_plane = np.r_[z_pca_axis[:2], - z_pca_axis[1::-1]]
    x_pca_plane.shape = (2, 2)
    y_pca_plane.shape = (2, 2)
    z_pca_plane.shape = (2, 2)
    ax.plot_surface(x_pca_plane, y_pca_plane, z_pca_plane)
    ax.w_xaxis.set_ticklabels([])
    ax.w_yaxis.set_ticklabels([])
    ax.w_zaxis.set_ticklabels([])


elev = -40
azim = -80
plot_figs(1, elev, azim)

elev = 30
azim = 20
plot_figs(2, elev, azim)

plt.show()




# http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html

pca.explained_variance_
Out[77]: array([ 1.33085609,  0.24521858,  0.08971764])

pca.explained_variance_ratio_
Out[78]: array([ 0.67600653,  0.12455844,  0.04557195])

pca.components_
Out[79]: 
array([[ -7.53485151e-01,   1.91792812e-01,   7.54594030e-02,
          1.00894201e-01,   1.56818634e-03,   6.71638404e-03,
          1.65033620e-02,   3.60550802e-01,  -3.39680733e-05,
         -4.30074314e-05,  -9.91609842e-03,   1.38777663e-04,
         -1.16872358e-02,   3.77976812e-06,  -3.74979175e-04,
         -1.99604859e-02,  -1.76519115e-06,   8.02956117e-04,
          1.77905534e-03,  -3.37560119e-05,   4.98227709e-06,
         -3.61930334e-06,  -7.97852133e-04,  -3.31907718e-03,
         -1.03111964e-04,   5.66894937e-03,  -3.77097358e-05],
       [  3.14719253e-01,  -1.13554664e+00,  -4.30830644e-01,
         -4.59054576e-01,  -1.11680384e-02,   1.98418243e-01,
          3.78841244e-02,   1.48557828e+00,   2.13947280e-04,
         -2.43522158e-03,   2.82203798e-02,   2.06721516e-04,
          1.49457316e-03,   1.25967611e-04,   5.22233467e-03,
          1.36015390e-01,   1.26930772e-05,  -2.61106693e-03,
          1.45739418e-03,   8.34448801e-05,   3.14435540e-04,
         -1.45221744e-05,   2.20154011e-02,  -3.10844222e-02,
         -6.90902239e-04,  -1.56425219e-01,  -9.65331393e-05],
       [ -7.94487760e-02,   5.94045216e-02,   9.49620202e-02,
         -5.90972824e-02,  -3.33644833e-02,  -7.69205402e-02,
          1.10647866e-01,  -1.61833255e-02,  -7.66373994e-04,
         -2.84907172e-02,   1.20861228e+00,  -4.75038500e-04,
          1.33730379e+00,   5.28123283e-05,  -1.44490802e-01,
          2.38366532e+00,  -1.08252794e-04,  -4.25011015e-02,
         -4.64171712e-01,  -3.93802102e-03,  -1.26787638e-03,
         -2.05091253e-04,  -1.93713960e-01,  -4.76083618e-02,
         -1.39645412e-03,   1.37638866e+00,  -2.49435431e-04]])

pca.components_.shape
Out[80]: (3, 27)

pca.components_[0]
Out[81]: 
array([ -7.53485151e-01,   1.91792812e-01,   7.54594030e-02,
         1.00894201e-01,   1.56818634e-03,   6.71638404e-03,
         1.65033620e-02,   3.60550802e-01,  -3.39680733e-05,
        -4.30074314e-05,  -9.91609842e-03,   1.38777663e-04,
        -1.16872358e-02,   3.77976812e-06,  -3.74979175e-04,
        -1.99604859e-02,  -1.76519115e-06,   8.02956117e-04,
         1.77905534e-03,  -3.37560119e-05,   4.98227709e-06,
        -3.61930334e-06,  -7.97852133e-04,  -3.31907718e-03,
        -1.03111964e-04,   5.66894937e-03,  -3.77097358e-05])

pca.components_[0].max()
Out[82]: 0.36055080191728661

pca.components_[0].argmax()
Out[83]: 7

dataframe.columns[7]
Out[84]: '"MoyenneEducation"'

pca.components_[0].argsort()
Out[85]: 
array([ 0, 15, 12, 10, 23, 22, 14, 24,  9, 26,  8, 19, 21, 16, 13, 20, 11,
       17,  4, 18, 25,  5,  6,  2,  3,  1,  7])

dataframe.columns[1]
Out[86]: 'AgeChef'

dataframe.columns[3]
Out[87]: 'NombreMembres'

dataframe.columns[2]
Out[88]: 'NiveauInsructionChef'
