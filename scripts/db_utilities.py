"""Utilities for database extraction. """

from sqlalchemy import Column, create_engine, Table, MetaData
from sqlalchemy.sql import column, func

def zero_replace_null(column_obj):
    """Replaces NULL values in the database with 0's. """
    
    if isinstance(column_obj, basestring):
        col = column(column_obj)
    elif isinstance(column_obj, Column):
        col = column_obj
    else:
        raise ValueError('Only support str and Column as input')
        
    # FIXME: are we sure we want to replace by 0 and not NaN !!! This can
    # a serious impact on results.
    return func.if_(col.is_(None), 0, col)
    
    
class DbConnection(object):
    
    def __init__(self):
        self.meta = MetaData()
	# Please provide your own credentials for the database
        self.engine = create_engine(
            'mysql+pymysql://ddesclee:{}@127.0.0.1:8889/rdc_luki_draft'.format(PASSWORD)
        )
        self.conn = self.engine.connect()

    
    def get_table(self, table_name):
        return Table(
            table_name, self.meta, autoload=True, autoload_with=self.engine
        )
        
    def execute(self, query):
        return self.conn.execute(query)
        
  

      
