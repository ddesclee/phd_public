# -*- coding: utf-8 -*-
""" Selection of the spatial dataset. """

import fiona
import numpy as np
from pandas import DataFrame
from sqlalchemy.sql import select, column
from sqlalchemy.sql.expression import Label


from db_utilities import DbConnection

TARGET_FILE = '/Users/ddesclee/Desktop/Phd/Recherche/Data/SIG_Luki/Couches thématiques/Villages.shp'

def load_reference(by_name=False):
    """ Loads a the reference id's (IDEnquete and IDVillage. """
    db = DbConnection()
    
    enquetes = db.get_table('Enquetes')
    villages = db.get_table('Villages')
    
    village_column = column('NomVillage') if by_name else Label('IDVillage', column('Enquetes.IDVillage', is_literal=True))
    
    requete_spatial_info = select(
        columns=['IDEnquete', village_column],
        from_obj=[enquetes, villages],
        whereclause=enquetes.c.IDVillage == villages.c.IDVillage
    )
    
    ### Execute
    results = db.execute(requete_spatial_info)
    #columns = [str(col) for col in results.columns] 
    data = results.fetchall()
    village_name = 'NomVillage' if by_name else 'IDVillage'
    dataframe =  DataFrame(data, columns=['IDEnquete', village_name])
    
    return dataframe.set_index('IDEnquete')
    
def load_village_info():
    """ Loads a the reference id's (IDEnquete and IDVillage. """
    db = DbConnection()
    
    villages = db.get_table('Villages')
    
    request = select(
        columns=['IDVillage', 'NomVillage'],
        from_obj=villages
    )
    
    ### Execute
    results = db.execute(request)
    data = results.fetchall()
    dataframe =  DataFrame(data, columns=['id', 'nom'])
    
    return dataframe.set_index('id')


def add_village_name(df):
    """ Adds a column to the dataframe with the village name.
    
    Makes the assumption that the daframe index is the IDEnquete.
    
    """
    
    reference = load_reference(by_name=True)
    
    df['village'] = reference
    
    return df

def test_fiona():
    TARGET_FILE = '/Users/ddesclee/Desktop/Phd/Recherche/Data/SIG_Luki/Couches thématiques/Villages.shp'
    with fiona.drivers():
        with fiona.open(TARGET_FILE) as fh:
            for p in fh:
                print p
                
def get_column_type(column_dtype):
    type_ = column_dtype.type
    if type_ is np.float64:
        return 'float'
    elif type_ is np.int64:
        return 'int'
    else:
        raise ValueError('Unsupported column type')

def export_to_shapefile(dataframe, output_name, source_shp=TARGET_FILE):
    """Given a dataframe with an index based on IDVillage, exports all the
    columns as attribute of a shapefile. """
    skipped_columns = []
    if '.shp' not in output_name:
        target_file = '{}.shp'.format(output_name)
    else:
        target_file = output_name
        
    with fiona.drivers():
        with fiona.open(source_shp) as source:
            schema = source.schema.copy()
            # add support to extract colums name and type from the dataframe
            # manage only float and int
            for column in dataframe.columns:
                col_type = get_column_type(dataframe.dtypes[column])
                if col_type is None:
                    skipped_columns.append(column)
                schema['properties'][column] = col_type
            
            with fiona.open(
                target_file, 'w', driver=source.driver, schema=schema
            ) as sink:
                for point in source:
                    # loop over the exported columns 
                    id_village = int(point['properties']['id'])
                    
                    for column in dataframe.columns: 
                        if column in skipped_columns:
                            continue
                        else:
                            point['properties'][column] = dataframe.loc[id_village, column]
                        
                    sink.write(point)
                    
    return target_file
    

if __name__  == '__main__':
    
   ref = test_fiona()
   
   data_ref = load_reference()
   export_to_shapefile(data_ref, 'test_export')