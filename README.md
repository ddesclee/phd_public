This repository contains the material built during the writing of my PhD thesis.

Database
--------

The database is available as a SQL dump of the MySQL database used during my research. 
The data has been anonymized (names have been removed) before publication.

Licenses
--------

The database is licensed under the Open Data Commons Open Database License.
The scripts are licensed under the BSD 3-clause license.

Contacts
-------

For any questions, please contact me at ddesclee@fastmail.fm


Doriane Desclee

